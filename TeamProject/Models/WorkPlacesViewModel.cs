﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamProject.Models
{
    public class WorkPlacesViewModel
    {
        public int DoctorId { get; set; }
        public List<Address> WorkPlacesList { get; set; }
        public List<Work_Hours> WorkHoursList { get; set; }
        public List<SERVICE> ServicesList { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamProject.Models
{
    public class NewWorkplaceViewModel
    {
        public List<Address> AllAddresses { get; set; }
        public int AddressId { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string ClinicName { get; set; }
        public string PhoneReception { get; set; }
        public string ClinicEmail { get; set; }
    }
}
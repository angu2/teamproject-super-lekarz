﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamProject.Models
{
    public class DoctorProfileViewModel
    {
        public Doctor DoctorData { get; set; }
        public Picture ProfilePicture { get; set; }
    }
}
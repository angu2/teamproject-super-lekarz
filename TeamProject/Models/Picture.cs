﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamProject.Models
{
    public class Picture
    {
        public string ContentType { get; set; }
        public string Base64Data { get; set; }
    }
}
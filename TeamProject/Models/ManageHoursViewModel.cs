﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamProject.Models
{
    public class ManageHoursViewModel
    {
        public List<Work_Hours> WorkHoursList { get; set; }
        public int AddressId { get; set; }
        public int DoctorId { get; set; }
    }
}
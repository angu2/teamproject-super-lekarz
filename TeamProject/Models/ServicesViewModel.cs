﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamProject.Models
{
    public class ServicesViewModel
    {
        public List<SERVICE> ServicesList { get; set; }
        public int AddressId { get; set; }
    }
}
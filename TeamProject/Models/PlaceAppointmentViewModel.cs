﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamProject.Models
{
    public class PlaceAppointmentViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Speciality { get; set; }
        public string Photo { get; set; }
        public List<Address> WorkPlaces { get; set; }
        public List<Appointment> Appointments { get; set; }
        public List<SERVICE> Services { get; set; }
        public List<Work_Hours> WorkHours { get; set; }
        public int? chosenWorkPlaceId { get; set; }
        public int? chosenServiceId { get; set; }
        public string chosenDate { get; set; }
        public string chosenTime { get; set; }
        public List<string> availableHours { get; set; }
    }
}
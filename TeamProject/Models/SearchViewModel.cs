﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamProject.Models
{
    public partial class SearchViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Speciality { get; set; }
        public string Photo { get; set; }
        public List<Address> WorkPlaces { get; set; }
    }
    class SearchViewModelComparer : IEqualityComparer<SearchViewModel>
    {
        public bool Equals(SearchViewModel sm1, SearchViewModel sm2)
        {
            return (sm1.Id == sm2.Id);
        }

        public int GetHashCode(SearchViewModel model)
        {
            return model.Id;
        }
    }
}
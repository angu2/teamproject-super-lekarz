﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamProject.Models;

namespace TeamProject.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult DoctorRequests()
        {
            using (var db = new DoctorBaseEntities())
            {
                var model = new List<AspNetUsers>(); 
                model.AddRange((
                    from a in db.AspNetUsers.AsEnumerable()
                    from d in db.Data.AsEnumerable()
                    where a.Id == d.user_id
                    where d.Doctor_request
                    where d.Access_level == 1
                    select new AspNetUsers()
                    {
                        Id = a.Id,
                        Email = a.Email,
                        PhoneNumber = a.PhoneNumber,
                        Data = a.Data
                    }).ToList());
            return View(model);
            }
        }

        public ActionResult CancelDoctor(int dataId)
        {
            using (var db = new DoctorBaseEntities())
            {
                var result = db.Data.FirstOrDefault(data => data.data_id == dataId);
                result.Doctor_request = false;
                db.SaveChanges();
                return RedirectToAction("DoctorRequests", "Admin");
            }
        }

        public ActionResult SetDoctor(int dataId)
        {
            using (var db = new DoctorBaseEntities())
            {
                var result = db.Data.FirstOrDefault(data => data.data_id == dataId);
                result.Access_level = 2;
                db.SaveChanges();

                var doctor = new Doctor()
                {
                    data_id = dataId,
                    doctor_id = db.Database.SqlQuery<int>("SELECT CAST(NEXT VALUE FOR Doctor_Seq as INT)").First()
                };
                db.Doctor.Add(doctor);
                db.SaveChanges();

                return RedirectToAction("DoctorRequests", "Admin");
            }
        }
    }
}
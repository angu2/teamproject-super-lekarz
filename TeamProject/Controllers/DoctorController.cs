﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using TeamProject.Models;

namespace TeamProject.Controllers
{
    public class DoctorController : Controller
    {
        // GET: Doctor
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WorkPlaces()
        {
            using (var db = new DoctorBaseEntities())
            {
                var userId = User.Identity.GetUserId();
                var dataId = db.Data.Where(d => d.user_id == userId).Select(d => d.data_id).FirstOrDefault();
                var doctorId = db.Doctor.Where(doc => doc.data_id == dataId).Select(doc => doc.doctor_id).FirstOrDefault();
                var model = new WorkPlacesViewModel
                {
                    DoctorId = doctorId,
                    WorkPlacesList = (from a in db.Address.AsEnumerable() from wh in db.Work_Hours
                                      where a.address_id == wh.address_id
                                      where wh.doctor_id == doctorId
                                      select new Address
                                      {
                                          address_id = a.address_id,
                                          line_1 = a.line_1,
                                          line_2 = a.line_2,
                                          city = a.city,
                                          zip_code = a.zip_code,
                                          clinic_name = a.clinic_name,
                                          phone_reception = a.phone_reception,
                                          email = a.email
                                      }).Distinct(new AddressComparer()).ToList(),
                    WorkHoursList = (from wh in db.Work_Hours.AsEnumerable()
                                     where wh.doctor_id == doctorId
                                    select new Work_Hours()
                                    {
                                        work_hours_id = wh.work_hours_id,
                                        start_time = wh.start_time,
                                        end_time = wh.end_time,
                                        day = wh.day,
                                        address_id = wh.address_id,
                                        doctor_id = doctorId
                                    }).ToList(),
                    ServicesList = (from s in db.SERVICE.AsEnumerable()
                        where s.doctor_id == doctorId
                        select new SERVICE()
                        {
                            service_id = s.service_id,
                            doctor_id = s.doctor_id,
                            address_id = s.address_id,
                            name = s.name,
                            price = s.price,
                            duration = s.duration
                        }).ToList(),
                };
                return View(model);
            }
        }

        public ActionResult NewWorkplace()
        {
            using (var db = new DoctorBaseEntities())
            {
                var model = new NewWorkplaceViewModel()
                {
                    AllAddresses = db.Address.Select(address => address).ToList()
                };
                return View(model);
            }
        }

        public ActionResult AddWorkplace(NewWorkplaceViewModel newWorkplace)
        {
            if (!newWorkplace.Line1.IsNullOrWhiteSpace())
            {
                using (var db = new DoctorBaseEntities())
                {
                    var newAddress = new Address
                    {
                        address_id =
                            db.Database.SqlQuery<int>("SELECT CAST(NEXT VALUE FOR Address_Seq as INT)").First(),
                        line_1 = newWorkplace.Line1,
                        line_2 = newWorkplace.Line2,
                        clinic_name = newWorkplace.ClinicName,
                        city = newWorkplace.City,
                        zip_code = newWorkplace.ZipCode,
                        phone_reception = newWorkplace.PhoneReception,
                        email = newWorkplace.ClinicEmail
                    };
                    db.Address.Add(newAddress);
                    db.SaveChanges();
                    newWorkplace.AddressId = newAddress.address_id;
                }
            }

            using (var db = new DoctorBaseEntities())
            {
                var userId = User.Identity.GetUserId();
                var dataId = db.Data.Where(d => d.user_id == userId).Select(d => d.data_id).FirstOrDefault();
                var doctorId = db.Doctor.Where(doc => doc.data_id == dataId).Select(doc => doc.doctor_id)
                    .FirstOrDefault();
                var workHoursList = new List<Work_Hours>();
                for (var i = 1; i <= 7; i++)
                {
                    workHoursList.Add(new Work_Hours()
                    {
                        work_hours_id = db.Database.SqlQuery<int>("SELECT CAST(NEXT VALUE FOR Work_Hours_Seq as INT)").First(),
                        start_time = null,
                        end_time = null,
                        day = i,
                        address_id = newWorkplace.AddressId,
                        doctor_id = doctorId
                    });
                }
                db.Work_Hours.AddRange(workHoursList);
                db.SaveChanges();
            }

            return RedirectToAction("ManageHours", "Doctor", new {addressId = newWorkplace.AddressId});
        }

        public ActionResult ManageHours(int addressId)
        {
            using (var db = new DoctorBaseEntities())
            {
                int doctorId = GetDoctorId(db);

                var model = new ManageHoursViewModel()
                {
                    WorkHoursList = (from wh in db.Work_Hours.AsEnumerable()                //Syntax tworzenia nowego obiektu na podstawie danych z bazy
                                     where wh.doctor_id == doctorId
                                     where wh.address_id == addressId
                                     select new Work_Hours()
                                     {
                                         work_hours_id = wh.work_hours_id,
                                         start_time = wh.start_time,
                                         end_time = wh.end_time,
                                         day = wh.day,
                                         address_id = wh.address_id,
                                         doctor_id = doctorId
                                     }).ToList(),
                    AddressId = addressId,
                    DoctorId = doctorId
                };
                return View(model);
            }
        }

        private int GetDoctorId(DoctorBaseEntities db)
        {
            var userId = User.Identity.GetUserId();
            var dataId = db.Data.Where(d => d.user_id == userId).Select(d => d.data_id).FirstOrDefault();
            var doctorId = db.Doctor.Where(doc => doc.data_id == dataId).Select(doc => doc.doctor_id).FirstOrDefault();
            return doctorId;
        }

        [HttpPost]
        public ActionResult SubmitWorkHours(ManageHoursViewModel model, List<Work_Hours> workHoursList)
        {
            using (var db = new DoctorBaseEntities())
            {
                var addressId = model.AddressId;
                var doctorId = model.DoctorId;
                foreach (var item in model.WorkHoursList)
                {
                    var result = db.Work_Hours
                        .Where(doc => doc.doctor_id == doctorId).Where(addr => addr.address_id == addressId).FirstOrDefault(d => d.day == item.day); //Syntax pobierania istniejacej encji
                    result.start_time = item.start_time;
                    result.end_time = item.end_time;
                    db.SaveChanges();
                }
                return RedirectToAction("WorkPlaces", "Doctor");
            }
        }

        public ActionResult Services(int addressId)
        {
            using (var db = new DoctorBaseEntities())
            {
                int doctorId = GetDoctorId(db);

                var model = new ServicesViewModel()
                {
                    ServicesList = new List<SERVICE>(db.SERVICE.Where(doc => doc.doctor_id == doctorId)
                        .Where(addr => addr.address_id == addressId).ToList()),
                    AddressId = addressId
                };
                return View(model);
            }
        }

        public ActionResult Appointments()
        {
            using (var db = new DoctorBaseEntities())
            {
                var doctorId = GetDoctorId(db);
                var model = new List<Appointment>(db.Appointment.Where(doc => doc.SERVICE.doctor_id == doctorId).Include(app => app.Data).Include(app => app.SERVICE).ToList());
                return View(model);
            }
        }
        
        public ActionResult SubmitServices(ServicesViewModel model)
        {
            using (var db = new DoctorBaseEntities())
            {
                var doctorId = GetDoctorId(db);
                foreach (var item in model.ServicesList)
                {
                    var result = db.SERVICE.Where(doc => doc.doctor_id == doctorId).Where(addr => addr.address_id == item.address_id).FirstOrDefault(serv => serv.service_id == item.service_id);
                    result.name = item.name;
                    result.duration = item.duration;
                    result.price = item.price;
                    db.SaveChanges();
                }
                return RedirectToAction("WorkPlaces", "Doctor");
            }
        }

        public ActionResult AddService(int addressId)
        {
            using (var db = new DoctorBaseEntities())
            {
                var model = new SERVICE()
                {
                    service_id = db.Database.SqlQuery<int>("SELECT CAST(NEXT VALUE FOR SERVICE_Seq as INT)").First(),
                    address_id = addressId,
                    doctor_id = GetDoctorId(db),
                };
                return View(model);
            }
        }

        public ActionResult CreateService(SERVICE service)
        {
            using (var db = new DoctorBaseEntities())
            {
                db.SERVICE.Add(service);
                db.SaveChanges();
                return RedirectToAction("Services", "Doctor", new { addressId = service.address_id });
            }
        }

        public ActionResult DeleteWorkplace(int addressid)
        {
            using (var db = new DoctorBaseEntities())
            {
                var doctorId = GetDoctorId(db);
                var workHoursList = db.Work_Hours.Where(doc => doc.doctor_id == doctorId)
                    .Where(addr => addr.address_id == addressid).ToList();
                var servicesList = db.SERVICE.Where(doc => doc.doctor_id == doctorId)
                    .Where(addr => addr.address_id == addressid).ToList();

                db.Work_Hours.RemoveRange(workHoursList);
                db.SERVICE.RemoveRange(servicesList);
                db.SaveChanges();

                return RedirectToAction("WorkPlaces", "Doctor");
            }
        }

        public ActionResult RemoveService(int serviceId)
        {
            using (var db = new DoctorBaseEntities())
            {
                var service = db.SERVICE.FirstOrDefault(serv => serv.service_id == serviceId);
                
                db.SERVICE.Remove(service);
                db.SaveChanges();

                return RedirectToAction("WorkPlaces", "Doctor");
            }
        }

        public ActionResult Profile()
        {
            using (var db = new DoctorBaseEntities())
            {
                var doctorId = GetDoctorId(db);
                var model = new DoctorProfileViewModel()
                {
                    DoctorData = db.Doctor.FirstOrDefault(doc => doc.doctor_id == doctorId),
                    ProfilePicture = null
                };
                if (model.DoctorData.photo != null)
                {
                    model.ProfilePicture = Newtonsoft.Json.JsonConvert.DeserializeObject<Picture>(model.DoctorData.photo);
                }
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult EditProfile(DoctorProfileViewModel model, HttpPostedFileBase profilePic)
        {
            using (var db = new DoctorBaseEntities())
            {
                var doctorId = GetDoctorId(db);
                var doctor = db.Doctor.FirstOrDefault(doc => doc.doctor_id == doctorId);
                if (profilePic != null)
                {

                    var uploadedFile = new byte[profilePic.InputStream.Length];
                    profilePic.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
                    var img = new WebImage(uploadedFile).Resize(400, 400, true, true);
                    uploadedFile = img.GetBytes();

                    var picToUpload = new Picture()
                    {
                        ContentType = profilePic.ContentType,
                        Base64Data = Convert.ToBase64String(uploadedFile)
                    };

                    doctor.photo = Newtonsoft.Json.JsonConvert.SerializeObject(picToUpload);
                }
                doctor.speciality = model.DoctorData.speciality;
                db.SaveChanges();

                return RedirectToAction("Profile", "Doctor");
            }
        }

        public ActionResult WebAPI_Test()
        {
            return View();
        }

        public ActionResult CancelVisit(int appointmentId)
        {
            using (var db = new DoctorBaseEntities())
            {
                var appointment = db.Appointment.FirstOrDefault(app => app.appointment_id == appointmentId);

                db.Appointment.Remove(appointment);
                db.SaveChanges();

                return RedirectToAction("Appointments", "Doctor");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using TeamProject.Models;

namespace TeamProject.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Projekt zespołowy";

            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Skontaktuj się z nami";

            return View();
        }

        public ActionResult Privacy()
        {
            ViewBag.Message = "Twoje dane są u nas bezpieczne";

            return View();
        }

        public ActionResult Search(string searchValue)
        {
            using (var db = new DoctorBaseEntities())
            {
                var model = new List<SearchViewModel>();
                model = (from addr in db.Address
                         from wh in db.Work_Hours
                         from doc in db.Doctor
                         from dat in db.Data
                         where wh.address_id == addr.address_id
                         where wh.doctor_id == doc.doctor_id
                         where dat.data_id == doc.data_id
                         where dat.first_name.Contains(searchValue) || dat.last_name.Contains(searchValue) || doc.speciality.Contains(searchValue) || (addr.clinic_name + addr.line_1 + addr.line_2 + addr.zip_code + addr.city).Contains(searchValue)
                         select new SearchViewModel()
                         {
                             Id = doc.doctor_id,
                             FirstName = dat.first_name,
                             LastName = dat.last_name,
                             Photo = doc.photo,
                             Speciality = doc.speciality,
                         }).AsEnumerable().Distinct(new SearchViewModelComparer()).ToList();
                foreach (var item in model)
                {
                    item.WorkPlaces = (from a in db.Address.AsEnumerable()
                                        from wh in db.Work_Hours
                                        where a.address_id == wh.address_id
                                        where wh.doctor_id == item.Id
                                        select new Address
                                        {
                                            address_id = a.address_id,
                                            line_1 = a.line_1,
                                            line_2 = a.line_2,
                                            city = a.city,
                                            zip_code = a.zip_code,
                                            clinic_name = a.clinic_name,
                                            phone_reception = a.phone_reception,
                                            email = a.email
                                        }).Distinct(new AddressComparer()).ToList();
                }
                return View(model);
            }
        }

        public ActionResult PlaceAppointment(int doctorId)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            using (var db = new DoctorBaseEntities())
            {
                var model = (from addr in db.Address
                    from wh in db.Work_Hours
                    from doc in db.Doctor
                    from dat in db.Data
                    where doc.doctor_id == doctorId
                    where wh.address_id == addr.address_id
                    where wh.doctor_id == doc.doctor_id
                    where dat.data_id == doc.data_id
                    select new PlaceAppointmentViewModel()
                    {
                        Id = doc.doctor_id,
                        FirstName = dat.first_name,
                        LastName = dat.last_name,
                        Photo = doc.photo,
                        Speciality = doc.speciality
                    }).FirstOrDefault();
                model.WorkPlaces = (from a in db.Address.AsEnumerable()
                    from w in db.Work_Hours
                    where a.address_id == w.address_id
                    where w.doctor_id == doctorId
                    select new Address
                    {
                        address_id = a.address_id,
                        line_1 = a.line_1,
                        line_2 = a.line_2,
                        city = a.city,
                        zip_code = a.zip_code,
                        clinic_name = a.clinic_name,
                        phone_reception = a.phone_reception,
                        email = a.email
                    }).Distinct(new AddressComparer()).ToList();
                model.Appointments = (from s in db.SERVICE.AsEnumerable()
                    from a in db.Appointment.AsEnumerable()
                    where a.service_id == s.service_id
                    where s.doctor_id == doctorId
                    select new Appointment()
                    {
                        SERVICE = a.SERVICE,
                        appointment_date = a.appointment_date
                    }).ToList();
                model.Services = db.SERVICE.Where(serv => serv.doctor_id == doctorId).Include(serv => serv.Address).ToList();
                model.WorkHours = db.Work_Hours.Where(wh => wh.doctor_id == doctorId).ToList();
                return View(model);
            }
        }

        public ActionResult FinalizeAppointment(PlaceAppointmentViewModel model)
        {
            List<Appointment> appointmentsForDay = null;
            if (model.Appointments != null)
            {
                appointmentsForDay = model.Appointments.Where(app => app.appointment_date.ToString("yyyy-MM-dd") == model.chosenDate).ToList();
            }
            var service = model.Services.FirstOrDefault(serv => serv.service_id == model.chosenServiceId);
            var day = model.WorkHours.Where(w => w.day == (int)Convert.ToDateTime(model.chosenDate).DayOfWeek + 1 && w.address_id == model.chosenWorkPlaceId).FirstOrDefault();
            model.availableHours = new List<string>();
            var endOfDay = day.end_time.Value.TimeOfDay;
            for (var i = day.start_time.Value.TimeOfDay; i < day.end_time.Value.TimeOfDay; i = i.Add(TimeSpan.FromMinutes(30)))
            {
                var validTime = true;
                var periodStart = i;
                var periodEnd = i.Add(TimeSpan.FromMinutes((double) service.duration));
                if (appointmentsForDay != null)
                {
                    foreach (var appointment in appointmentsForDay)
                    {
                        var appointmentStart = appointment.appointment_date.TimeOfDay;
                        var appointmentEnd = appointment.appointment_date.TimeOfDay.Add(TimeSpan.FromMinutes((double)appointment.SERVICE.duration));

                        if ((appointmentStart < periodStart && appointmentEnd > periodEnd) || (periodStart < appointmentEnd && periodEnd > appointmentStart) || (appointmentStart > periodStart && appointmentEnd < periodEnd) || (appointmentStart < periodEnd && periodStart < appointmentEnd) || (periodEnd > endOfDay))
                        {
                            validTime = false;
                        }
                    }
                }

                if (validTime)
                {
                    model.availableHours.Add(i.ToString(@"hh\:mm"));
                }
            }
            return View(model);
        }

        public ActionResult SubmitAppointment(PlaceAppointmentViewModel model)
        {
            using (var db = new DoctorBaseEntities())
            {
                var userId = User.Identity.GetUserId();
                var patientId = db.Data.Where(d => d.user_id == userId).Select(d => d.data_id).FirstOrDefault();
                var appointmentDate = Convert.ToDateTime(Convert.ToDateTime(model.chosenDate) + TimeSpan.Parse(model.chosenTime));
                var appointment = new Appointment()
                {
                    appointment_id = db.Database.SqlQuery<int>("SELECT CAST(NEXT VALUE FOR Appointment_Seq as INT)")
                        .First(),
                    service_id = (int) model.chosenServiceId,
                    patient_id = patientId,
                    appointment_date = appointmentDate
                };
                db.Appointment.Add(appointment);
                db.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}